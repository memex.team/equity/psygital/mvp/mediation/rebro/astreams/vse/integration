# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

Bundler.require(:parsers, :rdbms_pg, :rubyparser, :orchestrator)

#class Discovery < Discovery1; end
module Env; 
  module Orchestrated; end;
end

module Bro
  module Orchestrated; module Catman; 
    module Funded;   module Design; end; end;
    module Financed; module Design; end; end; 
  end; end
end

module Env
  module Base
    class << self
      
      def stage
        res = ENV['DeployStage']
        res
      end         
            
      def apiDesc
        res =  {
            title: 'Integration API',
            description: 'Integration API',
            contact_name: 'Eugene Istomin',
            contact_email: 'e.istomin@memex.team',
            license: 'See License.md',
          }
        #
        res
      end
            
      def logLevel
        res = Discovery.new
        res._dev = Logger::DEBUG
        res._prod = Logger::INFO
        res
      end
      
      
      def api
        res = Discovery.new
        res._path = '/api'
        res
      end     
      
      def localStorage
        res = LMDB.new('/tmp/', { mapsize: 100000000, maxdbs: 30})
        res
      end
      
    end
  end
end

