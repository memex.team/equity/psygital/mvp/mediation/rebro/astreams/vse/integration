# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded    
    module Design::Сables
      class Workflow
        class << self
  
          def run hIn
            zeebe_client = ::Zeebe::Client::GatewayProtocol::Gateway::Stub.new("#{Env::Orchestrated::Catman.orchestrator._host}:#{Env::Orchestrated::Catman.orchestrator._port}", :this_channel_is_insecure)
            
            processRunArgs = {  
                                processDefinitionKey: "#{hIn._assortID}#{Time.now.strftime("%S%M%k%d")}".to_i,
                                bpmnProcessId: Env::Orchestrated::Catman.orchestrator._bpmnName, 
                                version: -1, 
                                variables: {assortID: hIn._assortID, withQuota: hIn._withQuota, stage: hIn._stage, force: hIn._force}.to_json
                              }
            begin
              run = zeebe_client.create_process_instance(Zeebe::Client::GatewayProtocol::CreateProcessInstanceRequest.new(processRunArgs))
            rescue
              deploy
              run = zeebe_client.create_process_instance(Zeebe::Client::GatewayProtocol::CreateProcessInstanceRequest.new(processRunArgs))   
            end
            
            run                  
          end
          
          
          def sendMessage hIn
            zeebe_client = ::Zeebe::Client::GatewayProtocol::Gateway::Stub.new("#{Env::Orchestrated::Catman.orchestrator._host}:#{Env::Orchestrated::Catman.orchestrator._port}", :this_channel_is_insecure)
            zeebe_client.publish_message(Zeebe::Client::GatewayProtocol::PublishMessageRequest.new(
              name: hIn[:name],
              correlationKey: hIn[:uniqkey],
              variables: hIn[:variables].to_json
                                                                                                    ))                
          end
          
          
          def deploy
            zeebe_client = ::Zeebe::Client::GatewayProtocol::Gateway::Stub.new("#{Env::Orchestrated::Catman.orchestrator._host}:#{Env::Orchestrated::Catman.orchestrator._port}", :this_channel_is_insecure)
              run = zeebe_client.deploy_process(Zeebe::Client::GatewayProtocol::DeployProcessRequest.new({processes: [{name: Env::Orchestrated::Catman.orchestrator._bpmnName, definition: file_content = File.open("#{Env::Orchestrated::Catman.info._financed}/7_findata/orchestrator/bpmn/current.bpmn", "r:UTF-8", &:read)}] }))
              
              ReBro::Events::App.info("Orchestrator workflow is updated")
          end
          
          ## PROTECTED
          protected
    
          def privateDef
            puts 222
          end         
        
        end
      end
    end
  end
end
