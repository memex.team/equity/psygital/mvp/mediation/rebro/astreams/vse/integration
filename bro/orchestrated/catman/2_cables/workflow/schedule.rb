# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded    
    module Design::Сables
      class Workflow
        class << self
  

          def genNextRunTimestamp hIn
            res = {}
            hIn._type
            hIn._assortID
            #
            now = Time.now
            dayDate = now.strftime("%d.%m.%Y")            
            dayTimeHours   = now.strftime("%k")
            dayTimeMinutes = now.strftime("%M")
            dayTimeSeconds = now.strftime("%S")
            dayTime = "#{dayTimeHours}:#{dayTimeMinutes}:#{dayTimeSeconds}"
            dayTimeInSeconds = dayTimeSeconds.to_i + (dayTimeMinutes.to_i * 60) + (dayTimeHours.to_i * 3600)
            
            today = Date.today
            #
            elementsCount = Financed::Design::Pylons::Dataset.getDatasetInfo(assortID: hIn._assortID)[:result][:c_plu].to_i
            #          
            
            args = { assortID: hIn._assortID, schedule: getSchedule(hIn._type), dayName: today.strftime("%A"), dayTimeInSeconds: dayTimeInSeconds, elementsCount: elementsCount, today: today }
            res = slotsFind(Discovery.new args)
            
            #res = 2019-10-02T08:09:40+02:00
            res
          end
          
          
          def getSchedule type
            res = Discovery.new
            schedule = jsonFileDiscovery(currRbFile: __FILE__, jsonFile: "#{Env::Orchestrated::Catman.info._financed}/7_findata/orchestrator/schedules/#{type}.json")
            schedule.each do |k, v|
              k.to_s.split('_').each do |day|
                res[day] = v
              end
            end
            res
          end            
          
          def slotsFind hIn
            
            res = ''
              if hIn._schedule.key? hIn._dayName
                v =  hIn._schedule[hIn._dayName]
                slotTimeStart = v._slots._base._start.split(':')
                slotTimeStartInSeconds = slotTimeStart[2].to_i + (slotTimeStart[1].to_i * 60) + (slotTimeStart[0].to_i * 3600)
                
                slotTimeEnd = v._slots._base._end.split(':')
                slotTimeEndInSeconds = slotTimeEnd[2].to_i + (slotTimeEnd[1].to_i * 60) + (slotTimeEnd[0].to_i * 3600)  
                
                slotTimeDelta = slotTimeEndInSeconds - slotTimeStartInSeconds
                
                args = { today: hIn._today, slotLimit: v._slots._base._slotLimit}
                unallocElementsCount = getUnallocElementsCount(Discovery.new args)
                
                if hIn._dayTimeInSeconds <= slotTimeStartInSeconds &&  unallocElementsCount >= hIn._elementsCount
                  #
                  runTime = Time.at(slotTimeStartInSeconds + rand(slotTimeDelta)).utc.strftime("%H:%M:%S")
                  args = { today: hIn._today, elementsCount: hIn._elementsCount}
                  makeSlotOrder(Discovery.new args)
                  
                  nowInSeconds = Time.now.to_i                  
                  pluSendTimestamp = "#{hIn._today.to_s}T#{runTime}+03:00"                  
                  pluSendTimestampInSeconds =  Time.parse(pluSendTimestamp).to_i
                  pluSendDuration = "PT#{(pluSendTimestampInSeconds - nowInSeconds)}S"
                  
                  desc  = "scheduled run: #{hIn._dayName}, #{hIn._today.to_s} at #{runTime}, free slots: #{unallocElementsCount}"
                  #
                  res = {desc: desc, pluSendTimestamp: pluSendTimestamp, pluSendDuration: pluSendDuration}
                else
                  Log::App.debug("Day is full, try run next day")
                  nextDay = hIn._today.next_day
                  nextDayName = nextDay.strftime("%A")
                  
                  args = { schedule: hIn._schedule, dayName: nextDayName, dayTimeInSeconds: 0, elementsCount: hIn._elementsCount, today: nextDay }
                  res = slotsFind(Discovery.new args)
                end
                
              else
                Log::App.debug("Day not in schedule, try run next day (#{hIn._dayName})")                
                nextDay = hIn._today.next_day
                nextDayName = nextDay.strftime("%A")                
                args = { schedule: hIn._schedule, dayName: nextDayName, dayTimeInSeconds: 0, elementsCount: hIn._elementsCount, today: nextDay }
                res = slotsFind(Discovery.new args)
              end
            
            res
          end
          
          def getUnallocElementsCount hIn
            localStorage = ::Env::Base.localStorage
            scheduleDB = localStorage.database('schedule', create: true)            
            if scheduleDB["plu::slotLimit::#{hIn._today.to_s}"] == nil
              scheduleDB ["plu::slotLimit::#{hIn._today.to_s}"] = hIn._slotLimit.to_s
            end
            
            scheduleDB["plu::slotLimit::#{hIn._today.to_s}"].to_i
          end
          
          def makeSlotOrder hIn
            localStorage = ::Env::Base.localStorage
            scheduleDB = localStorage.database('schedule', create: true)            
            scheduleDB["plu::slotLimit::#{hIn._today.to_s}"] = (scheduleDB["plu::slotLimit::#{hIn._today.to_s}"].to_i - hIn._elementsCount).to_s
          end          
          
          ## PROTECTED
          protected
    
          def privateDef
            puts 222
          end         
        
        end
      end
    end
  end
end
