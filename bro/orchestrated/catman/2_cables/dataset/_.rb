# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded    
    module Design::Сables
      class Dataset
        class << self
    
          def db__sapPI hIn
            #
            hIn._apiArgs._stage = :prod unless hIn._apiArgs.key?('stage')
            #
            dbMapping = jsonFileDiscovery(currRbFile: __FILE__, jsonFile: "#{Env::Orchestrated::Catman.info._financed}/7_findata/catman/mappings/db.json")           
            sapPIMapping = jsonFileDiscovery(currRbFile: __FILE__, jsonFile: "#{Env::Orchestrated::Catman.info._financed}/7_findata/sapPI/mappings/sendPostfix.json")
            #
            flowType = dbMapping[hIn._apiPath._type]._flowType
            flow = dbMapping[hIn._apiPath._type]._flow
            counters = dbMapping[hIn._apiPath._type]._counters
            #
            args = { db: hIn._apiArgs._db, assortID: hIn._apiPath._assortID, dbType: "#{flowType}_#{flow}" }
            from = Financed::Design::Pylons::Dataset.getFlowData(Discovery.new args)
            #
            enrich = dataQ__db(data: from[:result].clone, apiArgs: hIn._apiArgs, apiPath: hIn._apiPath)
            metadata = genMetadata(data: enrich, apiArgs: hIn._apiArgs, apiPath: hIn._apiPath, flowType: flowType, flow: flow, counters: counters)
            
            sapPIDataset = makeSapPIDataset(data: enrich, type: hIn._apiPath._type)
            
            if hIn._apiArgs._run == 'true'
              metadata[:sapPI] = Design::Pylons::Dataset.postToSapPI_async(flowType: flowType, stage: hIn._apiArgs._stage, type: sapPIMapping[hIn._apiPath._type], data: sapPIDataset)
              error unless metadata[:sapPI][:piRespCode] == 200 or metadata[:sapPI][:piRespCode] == 202
            end
            
            result = metadata                
            
            if hIn._apiPath.key?(:format) 
              case hIn._apiPath._format
              when 'db'
                result = from[:result]
              when 'is'
                result = enrich
              when 'pi'
                result = sapPIDataset                    
              when 'csv'
                result = { rawData: from[:result][dbMapping[hIn._apiPath._type][:flow]], type: hIn._apiPath._type, contentType: 'text/csv' }
              when 'meta'
                result = metadata
              else
                result = {error: "no such format"}
              end
            end                
            
            
            result.key?(:rawData) ? res = result : res = { status: 200, result: result }
            
  #                 verify = ReBro::MMP::Verify.model(model: from[:result])
  #                 if verify == true
  #                   res = _to_sapPI(model: from, shape: hIn[:params][:shape])
  #                 else
  #                   res = { status: 500, perf: {}, result: verify}
  #                 end
            #
            res
          end
          
          ## PROTECTED
          protected
    
          def dataQ__db  hIn         
            Log::App.info(hIn[:apiPath])
            hIn[:data][:assortID] = hIn[:apiPath]._assortID.to_i
            hIn[:data]['mart'].upcase!
            
            hIn[:data]
          end    
          
          
          def genMetadata hIn
            meta = {}
            meta[:stage] = hIn[:apiArgs]._stage
            meta[:flowType] = hIn[:flowType]
            meta[:flow] = hIn[:flow]
            meta[:flowDirection] = :toPI
            meta[:assortId] = hIn[:apiPath]._assortID.to_i
            meta[:msgID] = hIn[:data]['msgID']
            meta[:cdtID] = hIn[:data]['cdtID'] if hIn[:data].key?('cdtID')                
            meta[:msgTimestamp] = hIn[:data]['timestamp']
            meta[:timestamp] = DateTime.now.strftime('%Q').to_i
            meta[:cdtName] = hIn[:data]['cdtName'] if hIn[:data].key?('cdtID')
            meta[:assortmentStatus] = hIn[:data]['assortmentStatus']
            meta[:email_list] = hIn[:data]['email_list'] if hIn[:data].key?('email_list')
            meta[:outdated] = hIn[:data]['outdated'] if hIn[:data].key?('outdated')
            meta[:counters] = {}  
                                #
            hIn[:counters].each do |metric|
              
              if hIn[:data][metric] == nil
                meta[:counters][metric] = 0
              else
                hIn[:data][metric].compact!       
                meta[:counters][metric] = hIn[:data][metric].count
              end
            end
            #
            meta
          end
          
          def makeSapPIDataset hIn

            hIn[:data].delete('assortmentStatus')
            
            case hIn[:type]
            when 'ph4'
              hIn[:data].delete('outdated')
              hIn[:data].delete('email_list')
            when 'plu'
              hIn[:data].delete('outdated')
              hIn[:data].delete('email_list')
              hIn[:data]['csb2plu'].each do |csb2plu|
                csb2plu.delete('ph3')
                csb2plu.delete('ph4')
                csb2plu.delete('oldPh3')
                csb2plu.delete('oldPh4,')
                a = []
              end
            end
            #
            res = hIn[:data]
            res              
          end          
                
        
        end
      end
    end
  end
end
