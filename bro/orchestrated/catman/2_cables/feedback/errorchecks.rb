# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded    
    module Design::Сables
      class Feedback
        class << self
    
          def errorChecks hIn 
            res = Discovery.new
            #
            type = hIn._apiPath._type
            errorsMapping = jsonFileDiscovery(currRbFile: __FILE__, jsonFile: "#{Env::Orchestrated::Catman.info._financed}/7_findata/sapPI/mappings/feedbacksErrorLevels.json")                       
            logPathRaw = "/tmp/integration/fromPI/#{type}/#{hIn._apiArgs._msgID}"
            sources = ['mdm', 'erp']
            #
            sources.each do |source|
              feedback = jsonFileDiscovery(currRbFile: __FILE__, jsonFile: "#{logPathRaw}/#{source}.json")
              l4Errors = errorsMapping[type][source]._l4
              
              feedback._errors.each do |id, props|
                l4Errors.each do |l4Error|
                  res[type][source]._errorlevel = 3 unless /#{l4Error}/.match (props._description)
                end
              end
              res[type][source]._errorlevel = 4 if res[type][source]._errorlevel == Discovery.new
            end
            
            #
            res.to_h
          end
          
          ## PROTECTED
          protected
    
          def privateDef
            puts 222
          end         
        
        end
      end
    end
  end
end
