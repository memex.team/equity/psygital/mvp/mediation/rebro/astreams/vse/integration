# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded    
    module Design::Сables
      class Feedback
        class << self
    
          def sendEmail hIn           
            mail = Discovery.new
            mail._stage = hIn._apiArgs._stage
            mail._to   = Env::Orchestrated::Catman.mailSend._to
            mail._from = Env::Orchestrated::Catman.mailSend._from
            #
            args = { mail: mail, params: hIn}
            send("mail#{hIn._apiPath._type.capitalize}".to_sym, (Discovery.new args))
            if hIn._apiArgs._stage == 'dev'
              res = mail
            else            
              Design::Pylons::Feedback.sendEmail(mail)
              res = {msg: "OK"}
            end
            res
          end   
          
          def mailStart hIn
            hIn._mail._subject = "Catman5: стартовала интеграция для ВПА #{hIn._params._apiArgs._assortID}"
            hIn._mail._body = "<!DOCTYPE html>
                    <head>
                    <meta charset='utf-8'>
                    <title>Catman5 Integration</title>
                    </head>         
                    <body>
                    <h2>Интеграционный микросервис Catman5</h2>

                    Стартовала интеграция по ВПА #{hIn._params._apiArgs._assortID}
                    </body>"
          end
          
          def mailPluestimation hIn
            
            pluSendTimestamp = hIn._params._apiArgs._pluSendTimestamp
            pluSendDate = DateTime.parse(pluSendTimestamp).strftime("%d.%m в %k:%M:%S")
            
            hIn._mail._subject = "Catman5: PLU-интеграция для ВПА #{hIn._params._apiArgs._assortID} запланирована на #{pluSendDate}"
            hIn._mail._body = "<!DOCTYPE html>
                    <head>
                    <meta charset='utf-8'>
                    <title>Catman5 Integration</title>
                    </head>         
                    <body>
                    <h2>Интеграционный микросервис Catman5</h2>

                    PLU-интеграция для ВПА #{hIn._params._apiArgs._assortID} запланирована на #{pluSendDate}
                    </body>"
          end
          
          def mailSuccess hIn
            hIn._mail._subject = "Catman5 OK: Интеграция для ВПА #{hIn._params._apiArgs._assortID} окончена успешно"
            hIn._mail._body = "<!DOCTYPE html>
                    <head>
                    <meta charset='utf-8'>
                    <title>Catman5 Integration</title>
                    </head>         
                    <body>
                    <h2>Интеграционный микросервис Catman5</h2>

                    Интеграция по ВПА #{hIn._params._apiArgs._assortID} окончена успешно
                    </body>"
          end    
          
          def mailError hIn
            logPathErrorChecks = "/tmp/integration/fromPI/#{hIn._params._apiArgs._step}/#{hIn._params._apiArgs._msgid}"
            
            hIn._mail._subject = "Catman5 ERROR: Ошибки в интеграции ВПА #{hIn._params._apiArgs._assortID}"
            hIn._mail._body = "<!DOCTYPE html>
                    <head>
                    <meta charset='utf-8'>
                    <title>Catman5 Integration</title>
                    </head>         
                    <body>
                    <h2>Интеграционный микросервис Catman5</h2>

                    Ошибки в интеграции ВПА #{hIn._params._apiArgs._assortID} 
                    
                    Шаг интеграции: #{hIn._params._apiArgs._step} 
                    MSG ID на шаге интеграции: #{hIn._params._apiArgs._msgid} 
                    -----------
                    
                    <h3>Ошибки на стороне MDM:</h3>
                    #{JSON.pretty_generate(jsonFileDiscovery(currRbFile: __FILE__, jsonFile: "#{logPathErrorChecks}/mdm.json")._errors.to_h)}
                    
                    -----------
                    
                    <h3>Ошибки на стороне ERP:</h3>
                    #{JSON.pretty_generate(jsonFileDiscovery(currRbFile: __FILE__, jsonFile: "#{logPathErrorChecks}/erp.json")._errors.to_h)}
                    
                    -----------
                    </body>"
                    
            hIn._mail._attachments = { "error_#{hIn._params._apiArgs._assortID}_#{hIn._params._apiArgs._step}_mdm.json" => File.read("#{logPathErrorChecks}/mdm.json"),
                                       "error_#{hIn._params._apiArgs._assortID}_#{hIn._params._apiArgs._step}_erp.json" => File.read("#{logPathErrorChecks}/erp.json"),
                                     }            
                    
          end            
          
 
          
          ## PROTECTED
          protected
    
          def privateDef
            puts 222
          end         
        
        end
      end
    end
  end
end
