# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

################
require 'test/unit'
require 'json' 
require 'date'
require 'net/http'
require 'httparty'
require_relative '../bim_lib/adv/discovery1'
require_relative '../bim_lib/adv/gauge'
include Test::Unit::Assertions

class Discovery < Discovery1; end

h = Discovery.new
args = Discovery.new
scenarioStore = Gauge::DataStoreFactory.scenario_datastore

def timeCorrelation hIn
  now = DateTime.now.strftime('%Q').to_i
  deltaTimeRealInSec = ((now - hIn[:eventTimestampMs])/1000).round
  pp "  ## Time correlation: #{hIn[:timeIntervalFrom]} >= #{deltaTimeRealInSec} >= #{hIn[:timeIntervalTo]}"
  assert_operator hIn[:timeIntervalFrom].to_i, :>=, deltaTimeRealInSec
  assert_operator hIn[:timeIntervalTo].to_i,   :<=, deltaTimeRealInSec
end
################
