# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

######
require_relative '_init'
specStore = Gauge::DataStoreFactory.spec_datastore
######

step 'clear' do ||
  h = Discovery.new
  args = Discovery.new
end

######

step 'Подождать <stepDelay> секунд' do |stepDelay|
   sleep stepDelay.to_i
end

######

step 'Запустить workflow интеграции по ВПА <assortID>, получить ответ со статусом <workflow> и описанием <desc>' do |assortID, workflow, desc|
 url = "/api/orchestrated/catman/workflow/run?assortID=#{assortID}&stage=dev"
 res = apiGet(url: url)
 assert_equal(res._workflow, workflow)
 assert_equal(res._desc, desc)
end

######

step 'Получить подтверждение о том, что поток <flow> по ВПА <assortID> отправлен в интервале [(now - <timeIntervalFrom>), (now - <timeIntervalTo>)] секунд' do |flow, assortID, timeIntervalFrom, timeIntervalTo|
  url = "/api/orchestrated/catman/dataset/_/#{flow}/#{assortID}?run=true&stage=dev&showlog=lastcall"
  res = apiGet(url: url)
  specStore.put("#{flow}MsgID", res._msgID)  
  #
  timeCorrelation(eventTimestampMs: res._msgTimestamp.to_i, timeIntervalFrom: timeIntervalFrom, timeIntervalTo: timeIntervalTo)
  assert_operator res._sapPI._piRespCode, :==, 200
  
  case flow
  when 'cdt'  
    assert_operator res._counters._csb, :==, 1
  when 'ph4'  
    assert_operator res._counters._ph4, :==, 1    
  end  
end

######

step 'Отправить SAP PI Feedback от источника <source> по потоку <flow> с severity <severity>' do |source, flow, severity|

  url = "/api/orchestrated/catman/feedback/pi/dev"
  json = JSON.parse(File.read("#{File.dirname(File.expand_path(__FILE__))}/../../mockups/sappi/#{source.downcase}_#{flow}.json"))
  
  case source
  when 'ERP'
      flowMsgID = specStore.get("#{flow}MsgID").to_i
  when 'MDM'
      flowMsgID = specStore.get("#{flow}MsgID").to_s.rjust(36, "0").to_s
  end
  
  json['MESSAGEHEADER']['REQID'] = flowMsgID
  res = apiPost(url: url, json: json.to_json)  
  assert_equal(res._workflow, 'OK')
end

######

step 'Получить подтверждение о том, что потоку <flow> по ВПА <assortID> назначено время отправки +<sched> секунд' do |flow, assortID, sched|
    url = "/api/orchestrated/catman/workflow/schedule/#{flow}/#{assortID}?stage=dev&showlog=lastcall"
    res = apiGet(url: url)
    now = DateTime.now.strftime('%Q').to_i
    timeCorrelation(eventTimestampMs: res._timestamp.to_i, timeIntervalFrom: 10, timeIntervalTo: 2)
    
    assert_equal(res._schedule._pluSendTimestamp[2..-2], sched)
end


######

step 'Получить подтверждение о том, что email типа <eventType> по ВПА <assortID> был выслан на <emailTo>' do |eventType, assortID, emailTo|
end
