# Workflow AC & QC

|assortID|
|--------|
|1045    |


## Workflow normal run: severity 5

tags: test
* clear
* Запустить workflow интеграции по ВПА <assortID>, получить ответ со статусом "OK" и описанием "Стартовала интеграция по ВПА 1045 в контуре dev"
* Получить подтверждение о том, что email типа "start" по ВПА <assortID> был выслан на "integration@some.com"

* Подождать "20" секунд
* Получить подтверждение о том, что поток "cdt" по ВПА <assortID> отправлен в интервале [(now - "20"), (now - "0")] секунд
* Отправить SAP PI Feedback от источника "MDM" по потоку "cdt" с severity "5"
* Отправить SAP PI Feedback от источника "ERP" по потоку "cdt" с severity "5"

* Подождать "60" секунд
* Получить подтверждение о том, что поток "ph4" по ВПА <assortID> отправлен в интервале [(now - "60"), (now - "0")] секунд
* Отправить SAP PI Feedback от источника "MDM" по потоку "ph4" с severity "5"
* Отправить SAP PI Feedback от источника "ERP" по потоку "ph4" с severity "5"

* Подождать "20" секунд
* Получить подтверждение о том, что потоку "plu" по ВПА <assortID> назначено время отправки +"30" секунд
* Получить подтверждение о том, что email типа "pluEstimation" по ВПА <assortID> был выслан на "integration@some.com"
* Подождать "30" секунд

* Подождать "50" секунд
* Получить подтверждение о том, что поток "plu" по ВПА <assortID> отправлен в интервале [(now - "80"), (now - "0")] секунд
* Отправить SAP PI Feedback от источника "MDM" по потоку "plu" с severity "5"
* Отправить SAP PI Feedback от источника "ERP" по потоку "plu" с severity "5"

* Получить подтверждение о том, что email типа "success" по ВПА <assortID> был выслан на "integration@some.com"
___
After all

* clear
