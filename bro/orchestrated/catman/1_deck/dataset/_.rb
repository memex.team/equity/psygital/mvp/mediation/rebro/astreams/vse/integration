# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded  
    module Design::Deck
      class Dataset < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Discovery::ParamBuilder
        namespace "/_" do        
          
          
        #### GET ####        
          ## EXEC 
          #### GET ####        
          get '/:type/:assortID' do            
            return lastcall(env) if params._apiArgs._showlog == 'lastcall'

            Log::App.info("New reqest for '#{params._apiPath._type}': assortID '#{params._apiPath._assortID}', run=#{params._apiArgs._run}")
            res = Design::Сables::Dataset.db__sapPI params

            if res.key?(:rawData)
              content_type res[:contentType]
              stream ReBro::Microservices::Archestry::Integration::Catman::Transpose::StreamGenCSV.new data: res[:rawData], type: res[:type]
            else
              result ({ status: res[:status].to_i, res: res[:result], env: env })
            end                            
            
          end

          #
        end

      end
    end
  end
end
