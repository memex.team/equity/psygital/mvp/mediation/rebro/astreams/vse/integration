# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded  
    module Design::Deck
      class Feedback < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Discovery::ParamBuilder
        namespace "/events" do        
          
          
        #### GET ####        
          ## EXEC 
          get '/mail/:type' do
            res = Design::Сables::Feedback.sendEmail params
            resArgs = { res: res, env: env }
            #
            result resArgs  
          end

          #
        end

      end
    end
  end
end
