# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded  
    module Design::Deck
      class Feedback < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Discovery::ParamBuilder
        namespace "/pi" do        
          
          
        #### GET ####        
          ## EXEC 
          post '/:stage' do
            logString = "from '#{params._apiArgs._MESSAGEHEADER._TARGETSYSTEM}', ReqID '#{params._apiArgs._MESSAGEHEADER._REQID.to_i}', interface '#{params._apiArgs._MESSAGEHEADER._SERVICEINTERFACE}'"
            Log::App.info("New feedback #{logString}")
            begin
              metadata = Design::Pylons::Feedback.pi(params)
              Design::Сables::Workflow.sendMessage(name: "#{metadata._flow}-pi-feedback-#{metadata._source.downcase}", uniqkey: metadata._reqID.to_s, variables: metadata.to_h)
              resArgs =  { status: 200, res: { workflow: "OK", desc: '' }, env: env }          
            rescue => e
              metadata = { status: e, env: env }
              resArgs =  { status: 500, res: { workflow: "PROBLEM", desc: e }, env: env }
            end
            
            if resArgs[:status] != 200 && metadata[:status] != 0
              event = "## PROBLEM: "
            else
              event = "@OK: "
            end
            
            Log::App.info("#{event}: feedback #{logString} processed with HTTP status '#{resArgs[:status]}' and internal status '#{metadata[:status]}'") 
            #
            result resArgs          
          end

          #
        end

      end
    end
  end
end
