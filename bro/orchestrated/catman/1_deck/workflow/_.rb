# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded  
    module Design::Deck
      class Workflow < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Discovery::ParamBuilder
        namespace "/" do        
          
          
        #### GET ####        
          ## EXEC 
          get '/deploy' do
            res = Design::Сables::Workflow.deploy(params)
            resArgs = { res: res, env: env }
            #
            result resArgs              
          end
          
    
          get '/run' do
            params.key?('withQuota') ? withQuota = true : withQuota = false
            params.key?('stage') ? stage = params._apiArgs._stage : stage = Env::Base.stage
            params.key?('force') ? force = params._apiArgs._force : force = false
            
            args = { assortID: params._apiArgs._assortID.to_i, withQuota: withQuota, stage: stage, force: force }
            run =  Design::Сables::Workflow.run(Discovery.new args)
            
            if run['processDefinitionKey'].to_i > 0
              resArgs = { res: { workflow: "OK", desc: "Стартовала интеграция по ВПА #{params._apiArgs._assortID.to_i} в контуре #{stage}" }, env: env }
            else
              resArgs = { status: 500, res: { workflow: "FAIL", desc: run.to_h }, env: env }
            end
            #
            result resArgs 
          end                 
          
          get '/message/:name' do
            argsIn = Hashie::Mash.new params
            zeebe_client = Zeebe::Client::GatewayProtocol::Gateway::Stub.new("#{ReBro::Env.workflow[:zeeBeeHost]}:#{ReBro::Env.workflow[:zeeBeePort]}", :this_channel_is_insecure)                

            zeebe_client.publish_message(Zeebe::Client::GatewayProtocol::PublishMessageRequest.new(
              name: argsIn.name,
              correlationKey: argsIn.mkey,
              variables: {argsIn.mkey => argsIn.mvalue.to_i}.to_json
                                                                                                  ))
            resArgs = { res: argsIn, env: env }
            #
            result resArgs  
          end           

          #
        end

      end
    end
  end
end
