# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded  
    module Design::Deck
      class Workflow < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Discovery::ParamBuilder
        namespace "/schedule" do        
          
          
        #### GET ####        
          ## EXEC 
          get '/:type/:assortID' do            
            return lastcall(env) if params._apiArgs._showlog == 'lastcall'   
            
            if params._apiArgs._stage == 'dev' or params._apiArgs._force == 'true'
              sched = { "desc": "scheduled at next 30 seconds", "pluSendTimestamp": "PT30S", "pluSendDuration": "PT30S" }
            else
              args  = { type: params._apiPath._type, assortID: params._apiPath._assortID }
              sched = Design::Сables::Workflow.genNextRunTimestamp(Discovery.new args)
            end
            
            now = DateTime.now.strftime('%Q').to_i            
            res = { schedule: sched, timestamp: now}
            resArgs = { res: res, env: env }
            #
            result resArgs              
          end

          #
        end

      end
    end
  end
end
