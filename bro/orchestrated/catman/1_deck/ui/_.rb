# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded  
    module Design::Deck
      class Ui < Grape::API
        
        format :json
        version 'v1', using: :header, vendor: 'reBro_bim'
        include Grape::Extensions::Discovery::ParamBuilder
        namespace "/" do        
          
          
        #### GET ####        
          ## EXEC 
          get '/' do
            res = File.read("#{Env::Orchestrated::Catman.info._financed}/7_findata/ui/workflow/index.html")
            resArgs = { mime: 'text/html', res: res, log: false }
            #
            result resArgs            
          end    
          
          get '/:file' do
            res = File.read("#{Env::Orchestrated::Catman.info._financed}/7_findata/ui/workflow/#{params._apiPath._file}.html").html_safe
            resArgs = { mime: 'text/html', res: res, log: false }
            #
            result resArgs  
          end          
          
          get '/assets/:dir/:file' do
            res = File.read("#{Env::Orchestrated::Catman.info._financed}/7_findata/ui/workflow/#{params._apiPath._dir}/#{params._apiPath._file}.#{params._apiPath._format}")
            
            case params._apiPath._dir.to_sym
            when :css
              content_type 'text/css'
            when :js
              content_type 'application/javascript'
            when :images
              content_type 'image/jpeg'
            end
            #
            stream Grape::StreamBinary.new res
          end            

          #
        end

      end
    end
  end
end
