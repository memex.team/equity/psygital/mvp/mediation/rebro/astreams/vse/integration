# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Env
  module Orchestrated::Catman
    class << self  
      
      def info
        res = Discovery.new   
        res._p1 = 'orchestrated'
        res._p2 = 'catman'
        baseBath = "#{File.dirname(File.expand_path(__FILE__))}/../../../../"
        res._funded   = "#{baseBath}/fun/bro/#{res._p1}/#{res._p2}"
        res._financed = "#{baseBath}/fin/bro/#{res._p1}/#{res._p2}"
        #res._class = "#{res._p1.capitalize}::#{res._p2.capitalize}".constantize
        res
      end           
      
      def orchestrator
        res = Discovery.new    
        res._host  = ENV['Orchestrated_Catman_Orchestrator_ZeeBe_Host']
        res._port = ENV['Orchestrated_Catman_Orchestrator_ZeeBe_Port']
        res._bpmnName = ENV['Orchestrated_Catman_Orchestrator_ZeeBe_BPMNName']
        res
      end        

      def sapPI(stage)
        res = Discovery.new  
        case stage
        when :dev      
          res._uriJ3     = ENV['Orchestrated_Catman_SapPI_Dev_URIJ3']
          res._uriQuotes = ENV['Orchestrated_Catman_SapPI_Dev_URIQoutes']
          res._user      = ENV['Orchestrated_Catman_SapPI_Dev_User']
          res._password  = ENV['Orchestrated_Catman_SapPI_Dev_Password']
          res._timeout   = 120

        when :test      
          res._uriJ3     = ENV['Orchestrated_Catman_SapPI_Test_URIJ3']
          res._uriQuotes = ENV['Orchestrated_Catman_SapPI_Test_URIQoutes']
          res._user      = ENV['Orchestrated_Catman_SapPI_Test_User']
          res._password  = ENV['Orchestrated_Catman_SapPI_Test_Password']   
          res._timeout   = 180
          
        when :prod
          res._uriJ3     = ENV['Orchestrated_Catman_SapPI_Prod_URIJ3']
          res._uriQuotes = ENV['Orchestrated_Catman_SapPI_Prod_URIQoutes']
          res._user      = ENV['Orchestrated_Catman_SapPI_Prod_User']
          res._password  = ENV['Orchestrated_Catman_SapPI_Prod_Password']           
          res._timeout   = 180
        end
        res
      end       
          
      def mailSend
        res = Discovery.new    
        res._host   = ENV['Orchestrated_Catman_Mail_Host']
        res._port   = ENV['Orchestrated_Catman_Mail_Port']
        res._domain = ENV['Orchestrated_Catman_Mail_Domain']
        res._from = 'integration@x5.ru' #ENV['Orchestrated_Catman_Mail_From']
        res._to   = ['ev.istomin@x5.ru', 'Kaloy.Egiev@x5.ru', 'MVolkova@x5.ru'] #ENV['Orchestrated_Catman_Mail_To']
        res
      end        

      def postgresDB(stage)
        res = Discovery.new    
        #
        case stage
        when :dev
          res._host    = ENV.fetch('Orchestrated_Catman_PostgresDBHostDev', 'localhost')
          res._port    = ENV.fetch('Orchestrated_Catman_PostgresDBPortDev', '7433')
          res._dbname  = ENV.fetch('Orchestrated_Catman_PostgresDBNameDev')       
        when :test
          res._host    = ENV.fetch('Orchestrated_Catman_PostgresDBHostTest', 'localhost')
          res._port    = ENV.fetch('Orchestrated_Catman_PostgresDBPortTest', '7433')
          res._dbname  = ENV.fetch('Orchestrated_Catman_PostgresDBNameTest')
        when :demo
          res._host    = ENV.fetch('Orchestrated_Catman_PostgresDBHostDemo', 'localhost')
          res._port    = ENV.fetch('Orchestrated_Catman_PostgresDBPortDemo', '7433')
          res._dbname  = ENV.fetch('Orchestrated_Catman_PostgresDBNameDemo')
        when :prod
          res._host    = ENV.fetch('Orchestrated_Catman_PostgresDBHostProd', 'localhost')
          res._port    = ENV.fetch('Orchestrated_Catman_PostgresDBPortProd', '7433')
          res._dbname  = ENV.fetch('Orchestrated_Catman_PostgresDBNameProd')
        
        end     
        #
        res._user    = ENV.fetch('Orchestrated_Catman_PostgresDBUser')
        res._pass    = ENV.fetch('Orchestrated_Catman_PostgresDBPassword')          
        #
        res
      end                       
        
    end
  end
end
