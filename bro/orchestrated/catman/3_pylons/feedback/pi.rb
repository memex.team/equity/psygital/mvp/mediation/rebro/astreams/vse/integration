# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded  
    module Design::Pylons
      class Feedback
        class << self
  
          def pi hIn
            flowDirection = :fromPI
            timestamp = DateTime.now.strftime('%Q').to_i
            
            ##
            result = Discovery.new
            result._stage = hIn._apiPath._stage
            result._reqID = hIn._apiArgs._MESSAGEHEADER._REQID.to_i
            result._source = hIn._apiArgs._MESSAGEHEADER._TARGETSYSTEM.split('_')[0]
            result._target = hIn._apiArgs._MESSAGEHEADER._TARGETSYSTEM.split('_')[1]
            interfaceArray = hIn._apiArgs._MESSAGEHEADER._SERVICEINTERFACE.downcase.split('_')
                            
            if interfaceArray.count == 1
              result._flowType = 'cdtflow'
              result._flow = 'cdt'
            elsif interfaceArray.count == 2
              result._flowType = interfaceArray[0]
              
              case interfaceArray[1]
              when 'csb'
                result._flow = 'cdt'
              when 'ph4'
                result._flow = 'ph4'  
              when 'csb2plu'
                result._flow = 'plu'              
              end
            end
            
            result._status = 0
            result._errors
            hIn._apiArgs._ITEM.each do |item|
              status = item._STATUS.to_i
              if status > 0
                result._status += status            
                result._errors[item._GUID.to_i.to_s] = { status: status, description: item._DESCRIPTION } unless item._DESCRIPTION == 'При вызове BAPI возникла одна или несколько ошибок'
              end
            end
            
            logPathErrorChecks = "/tmp/integration/fromPI/#{result._flow}/#{result._reqID}"
            FileUtils.mkdir_p logPathErrorChecks              
            File.open("#{logPathErrorChecks}/#{result._source.downcase}.json", 'w') { |file| file.write(JSON.pretty_generate(result.to_h)) }
            
            
            #logPathFull = "/tmp/integration/#{flowDirection}/#{result._flowType}/#{result._flow}/"
            #logPathFullEvent = "/tmp/integration/#{flowDirection}/msgID/"         
            #logPathAggregated = "/tmp/integration/aggregated/#{result._flow}"                  
            
            #FileUtils.mkdir_p logPathFull  
            #FileUtils.mkdir_p logPathFullEvent
            #FileUtils.mkdir_p logPathAggregated
            
            #File.open("#{logPathFull}/#{result._source}_#{result._reqID}_orig_#{timestamp}.json", 'w') { |file| file.write(JSON.pretty_generate(hIn)) }
            #File.open("#{logPathFullEvent}/#{result._reqID}_orig.json", 'w') { |file| file.write(JSON.pretty_generate(hIn)) }
            
            #File.open("#{logPathFull}/#{result._source}_#{result._reqID}_meta_#{timestamp}.json", 'w') { |file| file.write(JSON.pretty_generate(result)) }   
            #File.open("#{logPathFullEvent}/#{result._reqID}_meta.json", 'w') { |file| file.write(JSON.pretty_generate(result)) }
            
            #reqFileName = "#{logPathAggregated}/#{result._reqID}.json"
            #if File.file?(reqFileName)
              #json = JSON.parse(File.read(reqFileName))
              #json[result._source] = result
              #File.open(reqFileName, 'w') { |file| file.write(JSON.pretty_generate(json)) }                  
            #else
              #File.open(reqFileName, 'w') { |file| file.write(JSON.pretty_generate(result._source => result)) }
            #end


            result
          end
        
        end
      end
    end
  end
end
