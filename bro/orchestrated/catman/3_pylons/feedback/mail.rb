# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded  
    module Design::Pylons
      class Feedback
        class << self
  
          def sendEmail hIn
                        
            hIn._attachments == false if hIn._attachments == Discovery.new
            Pony.mail(:to          => hIn._to, 
                      :from        => hIn._from, 
                      :subject     => hIn._subject,
                      :html_body   => hIn._body, 
                      #:attachments => hIn._attachments,
                      :via         => :smtp,
                      :via_options => {
                        :address        => Env::Orchestrated::Catman.mailSend._host,
                        :port           => Env::Orchestrated::Catman.mailSend._port,
                        :domain         => Env::Orchestrated::Catman.mailSend._domain
                      }
                    )  
            

          end
        
        end
      end
    end
  end
end
