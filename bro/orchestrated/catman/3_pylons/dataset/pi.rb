# frozen_string_literal: true
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © 2017-2021 Eugene Istomin
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#    Language = ruby
#    Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

module Bro::Orchestrated::Catman
  module Funded  
    module Design::Pylons
      class Dataset
        class << self
  
          require 'protocol/http/header/authorization'
          require 'async/http/internet'
          require 'async/http/client'     
          
          def postToSapPI_async hIn     
            res = {}
            sapPICreds = Env::Orchestrated::Catman.sapPI(hIn[:stage].to_sym)
            #
            case hIn[:flowType]
            when 'cdtflow'
              host = sapPICreds._uriJ3
            when 'quotesflow'
              host = sapPICreds._uriQuotes
            end
            #
            uri = "#{host}/si_#{hIn[:type]}_outb"

            Async do |task|
              # Make a new conn:
              conn = Async::HTTP::Internet.new
              
              # Prepare the request:
              headers = [
                ['accept', 'application/json'], 
                ['authorization', Protocol::HTTP::Header::Authorization.basic(sapPICreds._user, sapPICreds._password)],
                ['content-type', 'application/json']
              ]                
              body = [hIn[:data].to_json]
              #Log::App.debug(hIn[:data])              
              # Issues a POST request:
              # Request will timeout after n seconds
              task.with_timeout(sapPICreds._timeout) do
                response = conn.post(uri, headers, body)
                rspBody = response.read
                #Log::App.debug("status:  #{response.status}")
                #Log::App.debug("rspBody: #{rspBody}")

                res[:reqBodyBytesize] = body[0].bytesize             
                res[:piRespCode] = response.status
              end
              rescue Async::TimeoutError
                res[:reqBodyBytesize] = 0
                res[:piRespCode] = 504
                res[:error] = "The request timed out by (re)Bro"
            ensure
              
              conn&.close
            end     
            #
            res
          end
                
        
        end
      end
    end
  end
end
