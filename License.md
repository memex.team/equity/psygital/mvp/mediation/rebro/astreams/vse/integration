### Memex.Team Licensing policy
    Copyright © 2017-2021 Memex.Team
    Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
    and others Memex.Team concepts & products.
     
    An idea, a vision, concepts, architectures, approaches & metamodels in 
    tangible or intangible forms (hereinafter - "Memex.Team assets") 
    are protected by copyrights & trademarks.
     
    All source texts/codes as Memex.Team assets representation are licensed 
    under a Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)
    Refer to the http://creativecommons.org/licenses/by-sa/4.0 for details
     
    Feel free to use the power of Memex.Team Psygital!
    
    
### How to use     
Please add reference to a "Memex.Team Licensing policy" 
in all text/source file headers in a proper way:

#### 1) Short header (preferable):
```
#######    ########    #######    ########    #######    #######    #######
#      / / / /    License    \ \ \ \     
#   Copyright © {YEAR(s)} {AUTHOR}
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.    
#   This code is covered by a Memex.Team Licensing policy (License.md)
#######    ########    #######    ########    #######    #######    #######
```

#### 2) Full header:

```
#######    ########    #######    ########    #######    #######    #######
#    / / / /    License    \ \ \ \ 
#    
#   Copyright © {YEAR(s)} {AUTHOR}
#   Copyright © 2017-2021 Memex.Team
#   Designed using Psygital®, Archestry®, Valuers.Stream®, Transfer.Games®
#   and others Memex.Team concepts & products.
#     
#   An idea, a vision, concepts, architectures, approaches & metamodels in 
#   tangible or intangible forms (hereinafter - "Memex.Team assets") 
#   are protected by copyrights & trademarks.
#     
#   All source texts/codes as Memex.Team assets representation are licensed 
#   under a Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)
#   Refer to the http://creativecommons.org/licenses/by-sa/4.0 for details
#     
#   Feel free to use the power of Memex.Team Psygital!
#######    ########    #######    ########    #######    #######    #######
```
